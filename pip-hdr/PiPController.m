//
//  PiPController.m
//  pip-hdr
//
//  Created by umxprime on 12/10/2023.
//

#import "PiPController.h"
#import <AVKit/AVKit.h>

@interface PiPController () <AVPictureInPictureSampleBufferPlaybackDelegate, AVPictureInPictureControllerDelegate>
@property (nonatomic) CALayer *displayLayer;
@property (nonatomic) AVPictureInPictureController *pipCtrl;
@end

@implementation PiPController

- (instancetype)initWithDisplayLayer:(CALayer *)displayLayer
{
    self = [super init];
    if (!self) return nil;
    assert([AVPictureInPictureController isPictureInPictureSupported]);
    _displayLayer = displayLayer;
    return self;
}

- (void)prepare
{
    AVPictureInPictureControllerContentSource *source;
    if ([_displayLayer isKindOfClass:[AVSampleBufferDisplayLayer class]]) {
        AVSampleBufferDisplayLayer *layer = (AVSampleBufferDisplayLayer *)_displayLayer;
        source = [[AVPictureInPictureControllerContentSource alloc] initWithSampleBufferDisplayLayer:layer
                                                                                    playbackDelegate:self];
    }
    if ([_displayLayer isKindOfClass:[AVPlayerLayer class]]) {
        AVPlayerLayer *layer = (AVPlayerLayer *)_displayLayer;
        source = [[AVPictureInPictureControllerContentSource alloc] initWithPlayerLayer:layer];
    }
    assert(source);
    
    
    _pipCtrl = [[AVPictureInPictureController alloc] initWithContentSource:source];
    _pipCtrl.delegate = self;
    [_pipCtrl invalidatePlaybackState];
    assert(_pipCtrl);
    
    [_pipCtrl addObserver:self forKeyPath:@"isPictureInPicturePossible"
                  options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:(__bridge void *)self];
    [_pipCtrl addObserver:self forKeyPath:@"isPictureInPictureActive"
                  options:NSKeyValueObservingOptionNew context:(__bridge void *)self];
}

- (void)startPictureInPicture
{
    [_pipCtrl startPictureInPicture];
}

- (void)stopPictureInPicture
{
    [_pipCtrl stopPictureInPicture];
}

- (void)close {
    [_pipCtrl removeObserver:self forKeyPath:@"isPictureInPicturePossible"];
    [_pipCtrl removeObserver:self forKeyPath:@"isPictureInPictureActive"];
    _pipCtrl = nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (object==_pipCtrl) {
        if ([keyPath isEqualToString:@"isPictureInPicturePossible"]) {
            NSLog(@"isPictureInPicturePossible:%d", [change[NSKeyValueChangeNewKey] boolValue]);
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - AVPictureInPictureSampleBufferPlaybackDelegate

- (void)pictureInPictureController:(nonnull AVPictureInPictureController *)pictureInPictureController
         didTransitionToRenderSize:(CMVideoDimensions)newRenderSize
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureController:(nonnull AVPictureInPictureController *)pictureInPictureController
                        setPlaying:(BOOL)playing
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureController:(nonnull AVPictureInPictureController *)pictureInPictureController
                    skipByInterval:(CMTime)skipInterval
                 completionHandler:(nonnull void (^)(void))completionHandler
{
    NSLog(@"%s", __FUNCTION__);
    completionHandler();
}

- (BOOL)pictureInPictureControllerIsPlaybackPaused:(nonnull AVPictureInPictureController *)pictureInPictureController
{
    NSLog(@"%s", __FUNCTION__);
    return NO;
}

- (CMTimeRange)pictureInPictureControllerTimeRangeForPlayback:(nonnull AVPictureInPictureController *)pictureInPictureController
{
    NSLog(@"%s", __FUNCTION__);
    return CMTimeRangeMake(kCMTimeNegativeInfinity, kCMTimePositiveInfinity);
}

#pragma mark - AVPictureInPictureControllerDelegate

- (void)pictureInPictureControllerWillStartPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureControllerDidStartPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureController:(AVPictureInPictureController *)pictureInPictureController
failedToStartPictureInPictureWithError:(NSError *)error {
    NSLog(@"%s %@", __FUNCTION__, error);
}

- (void)pictureInPictureControllerWillStopPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureControllerDidStopPictureInPicture:(AVPictureInPictureController *)pictureInPictureController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)pictureInPictureController:(AVPictureInPictureController *)pictureInPictureController
restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:(void (^)(BOOL restored))completionHandler {
    NSLog(@"%s", __FUNCTION__);
    completionHandler(YES);
}
@end
