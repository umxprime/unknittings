//
//  AppDelegate.m
//  pip-hdr
//
//  Created by umxprime on 15/03/2023.
//

#import "AppDelegate.h"

#import "../PlayerController.h"

@interface AppDelegate ()
@property (weak) IBOutlet NSSegmentedControl *displayLayerSelection;

@property (strong) IBOutlet NSWindow *window;
@property (nonatomic) PlayerController *playerController;
@end

@implementation AppDelegate

- (IBAction)switchPIP:(id)sender {
    [_playerController startPictureInPicture];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSView *displayView = _window.contentView;
    displayView.wantsLayer = YES;
    
    [self startPlayback];
    [_window makeKeyAndOrderFront:self];
}

- (IBAction)displayLayerChanged:(id)sender {
    [self startPlayback];
}

- (void)startPlayback {
    NSURL *mediaURL;
    
    NSString *fileBasename = @"Coffee Run - Blender Open Movie - 900kbps - hevc - 1920x858";
    NSString *fileExtension = @"mp4";
    mediaURL = [[NSBundle mainBundle] URLForResource:fileBasename
                                       withExtension:fileExtension];
    
    assert(mediaURL);
    PlayerControllerRenderer renderer = kPlayerControllerRendererSampleBufferDisplayLayer;
    if (_displayLayerSelection.indexOfSelectedItem == 0) {
        renderer = kPlayerControllerRendererAVPlayerLayer;
    }
    
    [_playerController stopPlayback];
    [_playerController.displayLayer removeFromSuperlayer];
    
    _playerController = [[PlayerController alloc] initWithRenderer:renderer];
    [_playerController startPlayback:mediaURL];
    NSView *displayView = _window.contentView;
    _playerController.displayLayer.frame = displayView.layer.frame;
    [displayView.layer addSublayer:_playerController.displayLayer];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


- (BOOL)applicationSupportsSecureRestorableState:(NSApplication *)app {
    return YES;
}

@end
