You may find here the project I sent yesterday to Apple to get help figuring out why we get a Picture in Picture activation issue only on macOS, with the new AVSampleBufferDisplayLayer based vout in VLC.  

The project demonstrates : 
- how the same playback and picture in picture code, used on iOS and macOS, doesn't give the same behaviour
- given AVPlayerLayer is used, it works as expected on both iOS/macOS
- given AVSampleBufferDisplayLayer is used, it work only on iOS

It provides an app for each target :
- one targeting macOS that provide a convenient UI to switch easily between a AVPlayerLayer and a AVSampleBufferDisplayLayer
- one targeting iOS that only demonstrate AVSampleBufferDisplayLayer works as expected (can be switched easily to AVPlayerLayer by editing the code)