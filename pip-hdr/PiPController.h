//
//  PiPController.h
//  pip-hdr
//
//  Created by umxprime on 12/10/2023.
//

#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PiPController : NSObject

//- (instancetype)initWithSampleBufferDisplayLayer:(AVSampleBufferDisplayLayer *)layer;
//- (instancetype)initWithPlayerLayer:(AVPlayerLayer *)playerLayer;
- (instancetype)initWithDisplayLayer:(CALayer *)playerLayer;
- (void)prepare;
- (void)close;
- (void)startPictureInPicture;
- (void)stopPictureInPicture;
@end

NS_ASSUME_NONNULL_END
