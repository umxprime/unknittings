//
//  ViewController.m
//  pip-hdr-iOS
//
//  Created by umxprime on 24/04/2023.
//

#import "ViewController.h"
#import <AVFoundation/AVFAudio.h>

#import "../PlayerController.h"

@interface ViewController ()
@property (nonatomic) PlayerController *playerController;
@end

@implementation ViewController {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    assert(!error);
    
    NSURL *mediaURL;
    
    NSString *fileBasename = @"Coffee Run - Blender Open Movie - 900kbps - hevc - 1920x858";
    NSString *fileExtension = @"mp4";
    mediaURL = [[NSBundle mainBundle] URLForResource:fileBasename
                                       withExtension:fileExtension];
    
    assert(mediaURL);
    _playerController = [[PlayerController alloc] initWithRenderer:kPlayerControllerRendererSampleBufferDisplayLayer];
    [_playerController startPlayback:mediaURL];
    _playerController.displayLayer.frame = self.view.layer.frame;
    [self.view.layer addSublayer:_playerController.displayLayer];
}

@end
