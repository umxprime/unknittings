//
//  PlayerController.m
//  pip-hdr
//
//  Created by umxprime on 12/10/2023.
//

#import "PlayerController.h"
#import <AVFoundation/AVFoundation.h>
#import "PiPController.h"

@interface PlayerController() <AVPlayerItemOutputPullDelegate>

@property (nonatomic) AVSampleBufferDisplayLayer *sampleBufferDisplayLayer;
@property (nonatomic) AVPlayerLayer *playerLayer;

@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayerItem *item;

@property (nonatomic) AVPlayerItemVideoOutput *playerItemVideoOutput;
#if TARGET_OS_OSX
@property (nonatomic) CVDisplayLinkRef displayLink;
#endif
#if TARGET_OS_IPHONE
@property (nonatomic) CADisplayLink *displayLink;
#endif

@property (nonatomic) PiPController *pipController;

- (void)preparePIP;
- (void)presentPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end

#if TARGET_OS_OSX
static CVReturn renderCallback(CVDisplayLinkRef displayLink,
                               const CVTimeStamp *inNow,
                               const CVTimeStamp *inOutputTime,
                               CVOptionFlags flagsIn,
                               CVOptionFlags *flagsOut,
                               void *displayLinkContext)
{
    PlayerController *controller = (__bridge PlayerController *)(displayLinkContext);
    CMTime frameTime = CMTimeMake(inOutputTime->videoTime, inOutputTime->videoTimeScale);
    AVPlayerItemVideoOutput *output = controller.playerItemVideoOutput;
    CMTime time = [output itemTimeForHostTime:CMTimeGetSeconds(frameTime)];
    if ([output hasNewPixelBufferForItemTime:time]) {
        CVPixelBufferRef ref;
        ref = [output copyPixelBufferForItemTime:time
                              itemTimeForDisplay:nil];
        if (ref == NULL)
            return kCVReturnSuccess;
        [controller presentPixelBuffer:ref];
        [controller preparePIP];
        CVBufferRelease(ref);
    }
    
    
    return kCVReturnSuccess;
}
#endif

@implementation PlayerController

- (instancetype)initWithRenderer:(PlayerControllerRenderer)renderer
{
    self = [super init];
    if (!self) return nil;
    _renderer = renderer;
    return self;
}

- (void)startPlayback:(NSURL *)mediaURL
{
    assert(mediaURL);
    
    AVAsset *asset = [AVAsset assetWithURL:mediaURL];
    assert(asset);
    
    _item = [[AVPlayerItem alloc] initWithAsset:asset];
    
    _player = [AVPlayer new];
    assert(_player);
    
    [_item addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [_player replaceCurrentItemWithPlayerItem:_item];
    [_player play];
    
    switch (_renderer) {
        case kPlayerControllerRendererAVPlayerLayer:
        {
            _playerLayer = [AVPlayerLayer playerLayerWithPlayer:_player];
            assert(_playerLayer);
            _displayLayer = _playerLayer;
        }
            break;
        case kPlayerControllerRendererSampleBufferDisplayLayer:
        {
            _sampleBufferDisplayLayer = [[AVSampleBufferDisplayLayer alloc] init];
            assert(_sampleBufferDisplayLayer);
            _displayLayer = _sampleBufferDisplayLayer;
        }
        default:
            break;
    }
    assert(_displayLayer);
#if TARGET_OS_OSX
    _displayLayer.autoresizingMask = kCALayerWidthSizable | kCALayerHeightSizable;
#endif
}

- (void)stopPlayback
{
    [_pipController close];
    _pipController = nil;
    [self stopDisplayLink];
    [_player replaceCurrentItemWithPlayerItem:nil];
    [_item removeObserver:self forKeyPath:@"status"];
    _item = nil;
    _playerItemVideoOutput = nil;
    _player = nil;
    _displayLayer = nil;
}

- (void)startPictureInPicture
{
    [_pipController startPictureInPicture];
}

- (void)createVideoOutput
{
    id attributes = @{
        (__bridge NSString*)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_420YpCbCr10BiPlanarVideoRange),
        (__bridge NSString*)kCVPixelBufferIOSurfacePropertiesKey:@{},
        (__bridge NSString*)kCVPixelBufferMetalCompatibilityKey:@(YES)
    };
    _playerItemVideoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:attributes];
    [_playerItemVideoOutput setDelegate:self queue:nil];
    [_playerItemVideoOutput requestNotificationOfMediaDataChangeWithAdvanceInterval:0.01];
    [_item addOutput:_playerItemVideoOutput];
}

- (CMSampleBufferRef)sampleBufferFromPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    if (pixelBuffer == NULL) {
        return NULL;
    }
    CMSampleBufferRef sampleBuffer = NULL;
    CMVideoFormatDescriptionRef formatDesc = NULL;
    OSStatus err = CMVideoFormatDescriptionCreateForImageBuffer(kCFAllocatorDefault, pixelBuffer, &formatDesc);
    if (err != noErr) {
        return NULL;
    }
    CMSampleTimingInfo sampleTimingInfo = kCMTimingInfoInvalid;
      
    err = CMSampleBufferCreateReadyWithImageBuffer(kCFAllocatorDefault, pixelBuffer, formatDesc, &sampleTimingInfo, &sampleBuffer);
    CFRelease(formatDesc);
    if (err != noErr) {
        return NULL;
    }
    
    CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
    CFMutableDictionaryRef dict = (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
    CFDictionarySetValue(dict, kCMSampleAttachmentKey_DisplayImmediately, kCFBooleanTrue);
    
    return sampleBuffer;
}

- (void)presentPixelBuffer:(CVPixelBufferRef)pixelBuffer 
{
    CMSampleBufferRef sampleBuffer = [self sampleBufferFromPixelBuffer:pixelBuffer];
    if (sampleBuffer == NULL) {
        return;
    }
    
    [_sampleBufferDisplayLayer enqueueSampleBuffer:sampleBuffer];
    
    CFRelease(sampleBuffer);
}

#if TARGET_OS_OSX
- (void)prepareDisplayLink 
{
    CGDirectDisplayID   displayID = CGMainDisplayID();
    CVReturn            error = kCVReturnSuccess;
    error = CVDisplayLinkCreateWithCGDisplay(displayID, &_displayLink);
    if (error)
    {
        NSLog(@"DisplayLink created with error:%d", error);
        _displayLink = NULL;
    }
    CVDisplayLinkSetOutputCallback(_displayLink, renderCallback, (__bridge void *)self);
    CVDisplayLinkStart(_displayLink);
}

- (void)stopDisplayLink
{
    CVDisplayLinkStop(_displayLink);
    CVDisplayLinkRelease(_displayLink);
    _displayLink = NULL;
}

#endif
#if TARGET_OS_IPHONE
- (void)step:(CADisplayLink *)displayLink {
    CMTime time = [_playerItemVideoOutput itemTimeForHostTime:displayLink.timestamp];
    if ([_playerItemVideoOutput hasNewPixelBufferForItemTime:time]) {
        CVPixelBufferRef ref;
        ref = [_playerItemVideoOutput copyPixelBufferForItemTime:time
                                              itemTimeForDisplay:nil];
        if (ref == NULL)
            return;
        [self presentPixelBuffer:ref];
        [self preparePIP];
        CVBufferRelease(ref);
    }
}

- (void)prepareDisplayLink {
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(step:)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}
#endif

- (void)preparePIP 
{
    if (_pipController) {
        return;
    }
    
    switch (_renderer) {
        case kPlayerControllerRendererSampleBufferDisplayLayer:
            _pipController = [[PiPController alloc] initWithDisplayLayer:_sampleBufferDisplayLayer];
            break;
        case kPlayerControllerRendererAVPlayerLayer:
            _pipController = [[PiPController alloc] initWithDisplayLayer:_playerLayer];
            break;
        default:
            break;
    }
    
    assert(_pipController);
    [_pipController prepare];
}

- (void)playerItemStatusDidChange:(AVPlayerItemStatus)status 
{
    switch (status) {
        case AVPlayerItemStatusReadyToPlay:
        {
            if (_renderer == kPlayerControllerRendererSampleBufferDisplayLayer) {
                [self createVideoOutput];
                [self prepareDisplayLink];
            } else {
                [self preparePIP];
            }
            break;
        }
        default:
            break;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
    if (object==_item) {
        if ([keyPath isEqualToString:@"status"] &&
            [change[NSKeyValueChangeNewKey] isKindOfClass:[NSNumber class]]) {
            [self playerItemStatusDidChange:[((NSNumber *)change[NSKeyValueChangeNewKey]) integerValue]];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - AVPlayerItemOutputPullDelegate

- (void)outputMediaDataWillChange:(AVPlayerItemOutput *)sender 
{
#if TARGET_OS_OSX
    CVDisplayLinkStart(_displayLink);
#endif
}

@end
