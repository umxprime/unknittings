//
//  PlayerController.h
//  pip-hdr
//
//  Created by umxprime on 12/10/2023.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/CoreAnimation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, PlayerControllerRenderer) {
    kPlayerControllerRendererAVPlayerLayer,
    kPlayerControllerRendererSampleBufferDisplayLayer,
};

@interface PlayerController : NSObject

@property(nonatomic, readonly) PlayerControllerRenderer renderer;
@property(nonatomic, readonly) CALayer *displayLayer;

- (instancetype)initWithRenderer:(PlayerControllerRenderer)renderer;
- (void)startPlayback:(NSURL *)mediaURL;
- (void)stopPlayback;
- (void)startPictureInPicture;
@end

NS_ASSUME_NONNULL_END
