//
//  main.m
//  CFRunLoopsTests
//
//  Created by umxprime on 18/03/2022.
//

#import <Foundation/Foundation.h>
@interface ThreadObj : NSObject
@end
@implementation ThreadObj
@end
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
static void SourcePerform(void *info) {
    (void)info;
}
static void SourceSchedule(void *info, CFRunLoopRef rl, CFRunLoopMode mode) {
    (void)info;
}
static void SourceCancel(void *info, CFRunLoopRef rl, CFRunLoopMode mode) {
    (void)info;
}
static const void *SourceRetain(const void *info) {
    return NULL;
}
static void SourceRelease(const void *info) {
    (void)info;
}
static CFStringRef SourceCopyDescription(const void *info) {
    return CFSTR("source");
}
static Boolean SourceEqual(const void *info1, const void *info2) {
    return false;
}
static CFHashCode SourceHash(const void *info) {
    return 12345;
}
static void dispatch(CFRunLoopRef runloop, CFRunLoopMode runloop_mode, void(^block)(void)) {
    CFStringRef modes_cfstrings[] = {
        kCFRunLoopDefaultMode,
        runloop_mode,
    };
    CFArrayRef modes = CFArrayCreate(NULL, (const void **)modes_cfstrings,
            ARRAY_SIZE(modes_cfstrings),
            &kCFTypeArrayCallBacks);
    CFRunLoopPerformBlock(runloop, modes, ^{
        block();
    });
    CFRunLoopWakeUp(runloop);
    CFRelease(modes);
}

static void runLoopInMode(CFRunLoopMode runloop_mode) {
    CFRunLoopRef runloop = CFRunLoopGetCurrent();
    CFRunLoopSourceContext ctx = {
        .retain = SourceRetain,
        .release = SourceRelease,
        .copyDescription = SourceCopyDescription,
        .equal = SourceEqual,
        .hash = SourceHash,
        .schedule = SourceSchedule,
        .cancel = SourceCancel,
        .perform = SourcePerform
    };
    CFRunLoopSourceRef source = CFRunLoopSourceCreate(NULL, 0, &ctx);
    CFRunLoopAddSource(runloop, source, runloop_mode);
    CFRunLoopRunResult ret;
    do {
        ret = CFRunLoopRunInMode(runloop_mode, INFINITY, true);
        NSLog(@"result");
    }
    while (ret != kCFRunLoopRunStopped);
    CFRunLoopRemoveSource(runloop, source, runloop_mode);
    CFRelease(source);
}

int main(int argc, const char * argv[]) {
    CFRunLoopMode runloop_mode = CFSTR("loop");
    dispatch_async(dispatch_get_main_queue(), ^{
        CFRunLoopRef runloop = CFRunLoopGetCurrent();
        // Stack "loop" run loop run over the main run loop
        dispatch_async(dispatch_get_main_queue(), ^{
            runLoopInMode(runloop_mode);
        });
        // Try to print log messages from two different runloops
        // During 5 seconds only "loop" messages will be printed
        // Then stacked "default" messages that weren't processed by the
        // kCFRunLoopDefaultMode will be printed before the main run loop
        // continues to print both messages
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
            NSUInteger idx = 0;
            while (1) {
                dispatch(runloop, runloop_mode, ^{
                    NSLog(@"loop %lu", (unsigned long)idx);
                });
                dispatch(runloop, kCFRunLoopDefaultMode, ^{
                    NSLog(@"default %lu", (unsigned long)idx);
                });
                sleep(1);
                idx ++;
            }
        });
        // Stop "loop" run loop run
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC*5),
                       dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), ^{
            dispatch(runloop, runloop_mode, ^{
                NSLog(@"stop");
                CFRunLoopStop(runloop);
            });
        });
    });
    // Start main run loop
    CFRunLoopRun();
    return 0;
}
