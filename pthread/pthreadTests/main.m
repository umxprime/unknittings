//
//  main.m
//  pthreadTests
//
//  Created by umxprime on 22/03/2022.
//

#import <Foundation/Foundation.h>
#import <pthread.h>
#import <stdatomic.h>

struct data_t {
    atomic_bool finished;
    pthread_cond_t condvar;
    pthread_mutex_t lock;
    CFRunLoopRef runloop;
};

pthread_t threadA;
pthread_t threadB;

struct data_t data;

void* ThreadA(void* ctx) {
    struct data_t *data = (struct data_t *)ctx;
    while (!atomic_load(&data->finished)) {
        NSLog(@"run");
        usleep(500);
    }
    pthread_mutex_lock(&data->lock);
    CFRunLoopStop(data->runloop);
    pthread_mutex_unlock(&data->lock);
    return NULL;
}

void* ThreadB(void* ctx) {
    struct data_t *data = (struct data_t *)ctx;
    sleep(1);
    atomic_store(&data->finished, true);
    return NULL;
}

int main(int argc, const char * argv[]) {
    atomic_init(&data.finished, false);
    data.runloop = CFRunLoopGetCurrent();
    pthread_mutex_init(&data.lock, NULL);
    pthread_create( &threadA, 0, ThreadA, &data);
    pthread_create( &threadB, 0, ThreadB, &data);
    CFRunLoopRun();
    pthread_join(threadA, NULL);
    pthread_join(threadB, NULL);
    pthread_mutex_destroy(&data.lock);
    return 0;
}
