//
//  AppDelegate.m
//  CADisplayLinkTest
//
//  Created by umxprime on 05/05/2022.
//

#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

static void SourcePerform(void *info) {
    (void)info;
}
static void SourceSchedule(void *info, CFRunLoopRef rl, CFRunLoopMode mode) {
    (void)info;
}
static void SourceCancel(void *info, CFRunLoopRef rl, CFRunLoopMode mode) {
    (void)info;
}
static const void *SourceRetain(const void *info) {
    return NULL;
}
static void SourceRelease(const void *info) {
    (void)info;
}
static CFStringRef SourceCopyDescription(const void *info) {
    return CFSTR("source");
}
static Boolean SourceEqual(const void *info1, const void *info2) {
    return false;
}
static CFHashCode SourceHash(const void *info) {
    return 12345;
}



@interface AppDelegate ()

@end

@implementation AppDelegate
{
    dispatch_queue_t _displayLinkRunLoopQueue;
    CADisplayLink *_displayLink;
    NSRunLoop *_displayLinkRunLoop;
    __uint64_t _last_ca_target_ts;
    __uint64_t _last_ca_now_ts;
    __uint64_t _last_vsync;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    dispatch_queue_attr_t attributes = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL_WITH_AUTORELEASE_POOL, QOS_CLASS_USER_INTERACTIVE, 0);
    _displayLinkRunLoopQueue = dispatch_queue_create("org.videolan.displayLinkRunLoopQueue", attributes);
    
    __block NSRunLoop *displayLinkRunLoop;
    CADisplayLink *displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkUpdate:)];
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    dispatch_async(_displayLinkRunLoopQueue, ^{
        displayLinkRunLoop = [NSRunLoop currentRunLoop];
        dispatch_semaphore_signal(sem);
        
        CFRunLoopRef runloop = displayLinkRunLoop.getCFRunLoop;
        CFRunLoopSourceContext ctx = {
            .retain = SourceRetain,
            .release = SourceRelease,
            .copyDescription = SourceCopyDescription,
            .equal = SourceEqual,
            .hash = SourceHash,
            .schedule = SourceSchedule,
            .cancel = SourceCancel,
            .perform = SourcePerform
        };
        CFRunLoopSourceRef source = CFRunLoopSourceCreate(NULL, 0, &ctx);
        CFRunLoopAddSource(runloop, source, kCFRunLoopDefaultMode);
        
        [displayLinkRunLoop run];
    });
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    [displayLink addToRunLoop:displayLinkRunLoop forMode:NSDefaultRunLoopMode];
    
//    NSTimer *timer = [NSTimer timerWithTimeInterval:.001 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        __uint64_t system_now_monotonic = clock_gettime_nsec_np(CLOCK_MONOTONIC);
//        __uint64_t system_now_realtime = clock_gettime_nsec_np(CLOCK_REALTIME);
//        NSLog(@"avstats: [CADISPLAYLINK][TIMER_CLOCK_MONOTONIC] ts=%" PRId64 " pts=%" PRId64,
//              system_now_monotonic, system_now_realtime);
//    }];
//    [displayLinkRunLoop addTimer:timer forMode:NSDefaultRunLoopMode];
    
    _displayLink = displayLink;
    _displayLinkRunLoop = displayLinkRunLoop;
    
    return YES;
}


- (void)displayLinkUpdate:(CADisplayLink*)displayLink {
    __uint64_t now = clock_gettime_nsec_np(CLOCK_MONOTONIC);

    CFTimeInterval ca_current = [displayLink timestamp];
    CFTimeInterval ca_target = [displayLink targetTimestamp];

    __uint64_t ca_now_ts = clock_gettime_nsec_np(CLOCK_UPTIME_RAW);
    __uint64_t ca_current_ts = ca_current * 1e9;
    __uint64_t ca_target_ts = ca_target * 1e9;

    __uint64_t last_ca_target_ts = _last_ca_target_ts == 0 ?
        ca_current_ts : _last_ca_target_ts;
    __uint64_t clock_offset = ca_current_ts - ca_now_ts;
    __uint64_t vsync_length = ca_target_ts - last_ca_target_ts;
    _last_ca_target_ts = ca_target_ts;
    __uint64_t vsync = now + clock_offset + vsync_length;
    
    if (_last_ca_now_ts != 0) {
        if (ca_now_ts - _last_ca_now_ts < 16300000)
            NSLog(@"avstats: [RENDER][CADISPLAYLINK][EARLY] ts=%" PRId64 " ", now);
        if (ca_now_ts - _last_ca_now_ts > 16900000)
            NSLog(@"avstats: [RENDER][CADISPLAYLINK][LATE] ts=%" PRId64 " ", now);
    }
    if (_last_vsync != 0) {
        if (vsync - _last_vsync < 16300000)
            NSLog(@"avstats: [RENDER][CADISPLAYLINK][EARLYVSYNC] ts=%" PRId64 " ", now);
        if (vsync - _last_vsync > 16900000)
            NSLog(@"avstats: [RENDER][CADISPLAYLINK][LATEVSYNC] ts=%" PRId64 " ", now);
    }
    _last_ca_now_ts = ca_now_ts;
    _last_vsync = vsync;
    NSLog(@"avstats: [RENDER][CADISPLAYLINK] ts=%" PRId64 " "
          "clock_offset=%" PRId64 " ca_now_ts=%" PRId64 " vsync_length=%" PRId64 " prev_ts=%" PRId64 " target_ts=%" PRId64 " vsync=%" PRId64,
          now,
          clock_offset,
          ca_now_ts,
          vsync_length,
          ca_current_ts,
          ca_target_ts,
          vsync);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    __uint64_t system_now_monotonic = clock_gettime_nsec_np(CLOCK_MONOTONIC);
    NSLog(@"avstats: [APP][WILLRESIGNACTIVE] ts=%" PRId64, system_now_monotonic);
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    __uint64_t system_now_monotonic = clock_gettime_nsec_np(CLOCK_MONOTONIC);
    NSLog(@"avstats: [APP][DIDBECOMEACTIVE] ts=%" PRId64, system_now_monotonic);
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


@end
