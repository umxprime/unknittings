//
//  AppDelegate.h
//  CADisplayLinkTest
//
//  Created by umxprime on 05/05/2022.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

