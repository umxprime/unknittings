# Building for macOS
## Minimal requirements
- a computer running macOS
- Xcode
- Python 3

## Get vlc sources
Get them using git from 
https://code.videolan.org/videolan/vlc  
Checkout `3.0` branch to build vlc 3 or checkout `master` if you want to try vlc 4

This guide have been tested with `3.0` commit `a0eecd9ea891a561de59fc9a017b9455e2d336be`

## Environment

### VLC root path

`${VLC_ROOT_PATH}` will be used in this document to refer to your local vlc repository clone folder.
For convenience you may set an environment var pointing to your vlc directory.
```
% export VLC_ROOT_PATH="path/to/vlc"
```

`${VLC_ROOT_PATH}` will be used in this document to refer to the your local vlc repository folder
### Check your PATH
There can have issues with already compiled libraries being exposed from your `$PATH` env var like some you may have got from various package managers (macports, homebrew, etc...) so be sure to print your path then clean it more or less like this 
```
% echo $PATH
/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/MacGPG2/bin:/Library/Apple/usr/bin
% export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/MacGPG2/bin:/Library/Apple/usr/bin
```
In the toolchain section we will set a completely new PATH environment variable but if you want to append $PATH in any way, be sure to have a clean one like this :

```
% echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin
```

### Toolchain

We have to prepare the environment to use Xcode toolchain programs that will be used to build vlc

```
export SDKROOT=$(xcrun --show-sdk-path)
export AR="$(xcrun --find ar)"
export CC="$(xcrun --find clang)"
export CXX="$(xcrun --find clang++)"
export NM="$(xcrun --find nm)"
export OBJC="$(xcrun --find clang)"
export OBJCXX="$(xcrun --find clang++)"
export RANLIB="$(xcrun --find ranlib)"
export STRIP="$(xcrun --find strip)"
```

You may check if things went well with some newly set environment vars :
```
% echo $SDKROOT;%echo $CC
/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk
/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang
```

### Build/Host machines

Host machine is the architecture on which vlc will run.  
Build machine is the architecture on which the project is being configured and compiled.  
On macOS host machine and build machine can be either x86_64 or aarch64/arm64.  

This is important because actually vlc and its contribs are configured using autotools, which will need proper target triplets to pass as options to do cross compile properly.  
More details about autoconf target triplets [here](https://www.gnu.org/software/autoconf/manual/autoconf-2.68/html_node/Specifying-Target-Triplets.html)

Triplets will be referenced in this guide with `${BUILD_TRIPLET}` and `${HOST_TRIPLET}` vars and they'll be used later in contribs and vlc configuration steps.

As an example, if you're on arm64 Apple Silicon and want to build vlc to run it on the same architecture and system version, you may set triplets manually like this :

First you get your actual macOS kernel version in `${MACOS_KERNEL_VERSION}` with
```
% MACOS_KERNEL_VERSION=$(uname -r | cut -d. -f1)
```

Then you set triplet vars using
```
% BUILD_TRIPLET="aarch64-apple-darwin${MACOS_KERNEL_VERSION}"
% HOST_TRIPLET="aarch64-apple-darwin${MACOS_KERNEL_VERSION}"
```

Another example would be to write small shell logic to prepare the triplets.  
To have vlc run on Intel x86_64 architecture, we might do this :
```
vlc % HOST_ARCH="x86_64"
BUILD_ARCH=`uname -m | cut -d. -f1`
if [ "$BUILD_ARCH" = "arm64" ]; then
    BUILD_ARCH="aarch64"
fi
MACOS_KERNEL_VERSION=$(uname -r | cut -d. -f1)

BUILD_TRIPLET="$BUILD_ARCH-apple-darwin${MACOS_KERNEL_VERSION}"
HOST_TRIPLET="$HOST_ARCH-apple-darwin${MACOS_KERNEL_VERSION}"
```

Then you may check the resulting values and if you're on an Apple Silicon Mac you may get this :
```
% echo $BUILD_TRIPLET;echo $HOST_TRIPLET 
aarch64-apple-darwin21
x86_64-apple-darwin21
```

If we wanted to prepare triplets to have vlc run on Apple Silicon arm64 architecture, we should have used `HOST_ARCH="aarch64"` in the above steps.

## Tools

Tools are all the necessary build systems required to configure and build vlc.
They are located in the `extras/tools` subfolder of vlc's project root directory.

The bootstrap script located at `extras/tools/bootstrap` will check environment to check which packages have to be built and will generate the appropriate Makefile

So in order to build the build system tools you have to do :
```
% cd "${VLC_ROOT_PATH}/extras/tools"
% ./bootstrap
% make
```

If for any reason the tools have to be rebuilt you can do :
```
% cd "${VLC_ROOT_PATH}/extras/tools"
% make clean
% ./bootstrap
% make
```
If everything went well, you'll see the message `You are ready to build VLC and its contribs`.

The resulting binaries will be built in `extras/tools/build/bin` hence this path should be added to your `PATH` environment var.

```
% echo $PATH          
/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin
% export PATH="${PATH}:${VLC_ROOT_PATH}/extras/tools/build/bin"
% echo $PATH                                                                  
/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin:/path/to/vlc/extras/tools/build/bin
```

## Contribs

TARGET_OSX_VERSION=11.0

export CFLAGS="-Werror=partial-availability"
export CXXFLAGS="-Werror=partial-availability"
export OBJCFLAGS="-Werror=partial-availability"

export EXTRA_CFLAGS="-isysroot ${SDKROOT} -mmacosx-version-min=${TARGET_OSX_VERSION} -arch x86_64"
export EXTRA_LDFLAGS="-isysroot ${SDKROOT} -mmacosx-version-min=${TARGET_OSX_VERSION} -arch x86_64"

Some contribs like growl, breakpad and sparkle may need additional environment `XCODE_FLAGS` for their xcodebuild command :
```
export XCODE_FLAGS="MACOSX_DEPLOYMENT_TARGET=${TARGET_OSX_VERSION} -sdk ${SDKROOT} WARNING_CFLAGS=-Werror=partial-availability"
```

rm -Rf ./*; START_DATE=$(date "+%s");../bootstrap --host=${HOST_TRIPLET} --build=${BUILD_TRIPLET};make fetch;make -j 8;echo $START_DATE;date "+%s"

```
% cd "${VLC_ROOT_PATH}/contrib"
rm -Rf "contrib-${HOST_TRIPLET}"
mkdir -p "contrib-${HOST_TRIPLET}" && cd "contrib-${HOST_TRIPLET}"
START_DATE=$(date "+%s")
../bootstrap --build=$BUILD_TRIPLET --host=$HOST_TRIPLET
make list
make fetch
make -j 8
echo $START_DATE;date "+%s"
```

### Bootstrap
### List selected contribs
### Build