//
//  SceneDelegate.h
//  VideoRenderer
//
//  Created by umxprime on 04/07/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

