//
//  SampleBufferRenderer.h
//  VideoRenderer
//
//  Created by umxprime on 28/09/2022.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SampleBufferRendererView : UIView
- (AVSampleBufferDisplayLayer *)displayLayer;
@end

@interface SampleBufferRenderer : NSObject
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)rendererWithView:(SampleBufferRendererView *)view;
- (instancetype)initWithView:(SampleBufferRendererView *)view;

- (void)presentPixelBuffer:(CVPixelBufferRef)pixelBuffer;

@end

NS_ASSUME_NONNULL_END
