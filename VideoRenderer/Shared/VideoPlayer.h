//
//  VideoPlayer.h
//  VideoRenderer
//
//  Created by umxprime on 05/07/2022.
//

#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN
@class UIViewController;

@interface VideoPlayer : NSObject

@property(nonatomic) void (^didDecodePixelBuffer)(CVPixelBufferRef);

- (void)decode:(NSURL *)mediaURL;
@end

NS_ASSUME_NONNULL_END
