//
//  VideoPlayer.m
//  VideoRenderer
//
//  Created by umxprime on 05/07/2022.
//

#import "VideoPlayer.h"

@interface VideoPlayer () <AVPlayerItemOutputPullDelegate>

@end

@implementation VideoPlayer
{
    AVPlayer *_player;
    AVPlayerItem *_item;
    AVPlayerItemVideoOutput *_output;
    CADisplayLink *_displayLink;
    dispatch_queue_t _videoOutputQueue;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)createVideoOutput {
    id attributes = @{
        (__bridge NSString*)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32BGRA),
        (__bridge NSString*)kCVPixelBufferIOSurfacePropertiesKey:@{},
        (__bridge NSString*)kCVPixelBufferOpenGLCompatibilityKey:@(YES),
        (__bridge NSString*)kCVPixelBufferOpenGLESCompatibilityKey:@(YES),
        (__bridge NSString*)kCVPixelBufferMetalCompatibilityKey:@(YES)
    };
    _output = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:attributes];
    [_output setDelegate:self queue:nil];
    [_output requestNotificationOfMediaDataChangeWithAdvanceInterval:0.01];
    [_item addOutput:_output];
}

- (void)prepareDisplayLink {
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayUpdate:)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [_displayLink setPaused:YES];
}

- (void)playerItemStatusDidChange:(AVPlayerItemStatus)status {
    switch (status) {
        case AVPlayerItemStatusReadyToPlay:
        {
            [self prepareDisplayLink];
            [self createVideoOutput];
            break;
        }
        default:
            break;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _item) {
        if ([keyPath isEqualToString:@"status"] &&
            [change[NSKeyValueChangeNewKey] isKindOfClass:[NSNumber class]]) {
            [self playerItemStatusDidChange:[((NSNumber *)change[NSKeyValueChangeNewKey]) integerValue]];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)decode:(NSURL *)mediaURL {
    AVAsset *asset = [AVAsset assetWithURL:mediaURL];
    _item = [[AVPlayerItem alloc] initWithAsset:asset];
    _player = [AVPlayer new];
    [_item addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [_player replaceCurrentItemWithPlayerItem:_item];
    [_player play];
}

- (void)displayUpdate:(CADisplayLink *)displayLink {
    CFTimeInterval nextVSync = ([displayLink timestamp] + [displayLink duration]);
    CMTime time = [_output itemTimeForHostTime:nextVSync];
    if ([_output hasNewPixelBufferForItemTime:time]) {
        CVPixelBufferRef ref;
        ref = [_output copyPixelBufferForItemTime:time
                               itemTimeForDisplay:nil];
        if (ref == NULL)
            return;
        if (_didDecodePixelBuffer) {
            _didDecodePixelBuffer(ref);
        }
        CVBufferRelease(ref);
    }
}

- (void)outputMediaDataWillChange:(AVPlayerItemOutput *)sender {
    [_displayLink setPaused:NO];
}

@end
