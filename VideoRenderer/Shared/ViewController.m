//
//  ViewController.m
//  VideoRenderer
//
//  Created by umxprime on 04/07/2022.
//

#import "ViewController.h"
#import "VideoPlayer.h"
#import "SampleBufferRenderer.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet SampleBufferRendererView *rendererView;
@end

@implementation ViewController {
    VideoPlayer *_player;
    SampleBufferRenderer *_sampleBufferRenderer;
}

- (void)viewDidLoad {
    _player = [VideoPlayer new];
    SampleBufferRenderer *sampleBufferRenderer = [SampleBufferRenderer rendererWithView:_rendererView];
    _sampleBufferRenderer = sampleBufferRenderer;
    _player.didDecodePixelBuffer = ^(CVPixelBufferRef _Nonnull pixelBuffer) {
        [sampleBufferRenderer presentPixelBuffer:pixelBuffer];
    };
    
    NSURL *url = [NSURL URLWithString:@"http://127.0.0.1:8080/index.m3u8"];
    
    [_player decode:url];
    [super viewDidLoad];
}

@end
