//
//  SampleBufferRenderer.m
//  VideoRenderer
//
//  Created by umxprime on 28/09/2022.
//

#import "SampleBufferRenderer.h"
#import <CoreImage/CIFilterBuiltins.h>

@implementation SampleBufferRendererView

+ (Class)layerClass {
    return [AVSampleBufferDisplayLayer class];
}

- (AVSampleBufferDisplayLayer *)displayLayer {
    return (AVSampleBufferDisplayLayer *)self.layer;
}

@end

@implementation SampleBufferRenderer {
    BOOL _isFilterApplied;
    CVPixelBufferPoolRef _cvpxPool;
    CIContext *_context;
    EAGLContext *_glContext;
    SampleBufferRendererView *_view;
}

+ (instancetype)rendererWithView:(SampleBufferRendererView *)view {
    return [[self alloc] initWithView:view];
}

- (instancetype)initWithView:(SampleBufferRendererView *)view {
    self = [super init];
    if (self) {
        _view = view;
        _isFilterApplied = NO;
    }
    return self;
}

- (CVPixelBufferRef)applyFilter:(CVPixelBufferRef)pixelBuffer {
    if (pixelBuffer == NULL || _isFilterApplied == NO) {
        return NULL;
    }
    OSStatus err = noErr;
    CFDictionaryRef cvpxAttribs = CVPixelBufferCopyCreationAttributes(pixelBuffer);
    if (_cvpxPool != NULL) {
        CFDictionaryRef poolCvpxAttribs = CVPixelBufferPoolGetPixelBufferAttributes(_cvpxPool);
        if (poolCvpxAttribs != NULL &&
            CFEqual(poolCvpxAttribs, cvpxAttribs) == false) {
            CVPixelBufferPoolRelease(_cvpxPool);
            _cvpxPool = NULL;
        }
    }
    if (_cvpxPool == NULL) {
        err = CVPixelBufferPoolCreate(kCFAllocatorDefault,
                                      NULL,
                                      cvpxAttribs,
                                      &_cvpxPool);
    }
    CFRelease(cvpxAttribs);
    if (err != noErr) {
        return NULL;
    }
    
    CIImage *image = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    CIFilter *filter;
    
    CIFilter<CISharpenLuminance> *sharpenLuminance = [CIFilter sharpenLuminanceFilter];
    sharpenLuminance.sharpness = 5;
    sharpenLuminance.radius = 1;
    filter = sharpenLuminance;
    
    [filter setValue:image forKey:kCIInputImageKey];
    CVPixelBufferRef filteredPixelBuffer = NULL;
    err = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, _cvpxPool, &filteredPixelBuffer);
    if (err != noErr) {
        return NULL;
    }
    if (_context == nil) {
        _context = [CIContext context];
    }
    [_context render:filter.outputImage toCVPixelBuffer:filteredPixelBuffer];
    return filteredPixelBuffer;
}

- (CMSampleBufferRef)sampleBufferFromPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    if (pixelBuffer == NULL) {
        return NULL;
    }
    CMSampleBufferRef sampleBuffer = NULL;
    CMVideoFormatDescriptionRef formatDesc = NULL;
    OSStatus err = CMVideoFormatDescriptionCreateForImageBuffer(kCFAllocatorDefault, pixelBuffer, &formatDesc);
    if (err != noErr) {
        return NULL;
    }
    CMSampleTimingInfo sampleTimingInfo = kCMTimingInfoInvalid;
      
    err = CMSampleBufferCreateReadyWithImageBuffer(kCFAllocatorDefault, pixelBuffer, formatDesc, &sampleTimingInfo, &sampleBuffer);
    CFRelease(formatDesc);
    if (err != noErr) {
        return NULL;
    }
    
    CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
    CFMutableDictionaryRef dict = (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
    CFDictionarySetValue(dict, kCMSampleAttachmentKey_DisplayImmediately, kCFBooleanTrue);
    
    return sampleBuffer;
}

- (void)presentPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    CVPixelBufferRef presentedPixelBuffer = pixelBuffer;
    CVPixelBufferRef filtered = [self applyFilter:pixelBuffer];
    if (filtered != NULL) {
        presentedPixelBuffer = filtered;
    }
    
    CMSampleBufferRef sampleBuffer = [self sampleBufferFromPixelBuffer:presentedPixelBuffer];
    if (sampleBuffer == NULL) {
        return;
    }
    
    [_view.displayLayer enqueueSampleBuffer:sampleBuffer];
    
    CFRelease(sampleBuffer);
    if (filtered != NULL) {
        CFRelease(filtered);
    }
}
@end
