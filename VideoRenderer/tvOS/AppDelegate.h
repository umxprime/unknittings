//
//  AppDelegate.h
//  VideoRenderer
//
//  Created by umxprime on 27/09/2022.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

