Given `collectionView.allowsMultipleSelectionDuringEditing == true` and the related -[UIViewController setEditing:animated:] selector is called two times passing `YES` then `NO` to the Editing parameter.

    (lldb) thread backtrace
* thread #1, queue = 'com.apple.main-thread', stop reason = breakpoint 6.1
  * frame #0: 0x00000001021d718c VLC for iOS`MediaCategoryViewController.collectionView(collectionView=0x000000010910f200, indexPath=2 indices, self=0x0000000109970a00) at MediaCategoryViewController.swift:841:9
    frame #1: 0x00000001021d7220 VLC for iOS`@objc MediaCategoryViewController.collectionView(_:didSelectItemAt:) at <compiler-generated>:0
    frame #2: 0x000000018ebf2c58 UIKitCore`-[UICollectionView _selectItemAtIndexPath:animated:scrollPosition:notifyDelegate:deselectPrevious:performCustomSelectionAction:] + 892
    frame #3: 0x000000018ef578e4 UIKitCore`__60-[_UICollectionViewMultiSelectController _selectIndexPaths:]_block_invoke + 220
    frame #4: 0x000000018f11cdd0 UIKitCore`-[UIMultiSelectInteractionState ignoreSelectionChangedNotificationsWithBlock:] + 40
    frame #5: 0x000000018ef577d0 UIKitCore`-[_UICollectionViewMultiSelectController _selectIndexPaths:] + 324
    frame #6: 0x000000018ef57e40 UIKitCore`-[_UICollectionViewMultiSelectController updateSelectedIndexPaths:] + 308
    frame #7: 0x000000018ef575f4 UIKitCore`-[_UICollectionViewMultiSelectController _extendSelectionToPoint:] + 340
    frame #8: 0x000000018ef591cc UIKitCore`-[_UICollectionViewMultiSelectController multiSelectInteraction:toggleSelectionStateUpToPoint:] + 124
    frame #9: 0x000000018ecc750c UIKitCore`-[UIMultiSelectInteraction _updateCommonPan:] + 164
    frame #10: 0x000000018ecc735c UIKitCore`-[UIMultiSelectInteraction _handleCommonPanGestureStateChanged:] + 80
    frame #11: 0x000000018effe3fc UIKitCore`-[UIGestureRecognizerTarget _sendActionWithGestureRecognizer:] + 52
    frame #12: 0x000000018f007970 UIKitCore`_UIGestureRecognizerSendTargetActions + 112
    frame #13: 0x000000018f0045f8 UIKitCore`_UIGestureRecognizerSendActions + 352
    frame #14: 0x000000018f003bc0 UIKitCore`-[UIGestureRecognizer _updateGestureForActiveEvents] + 668
    frame #15: 0x000000018eff8150 UIKitCore`_UIGestureEnvironmentUpdate + 2068
    frame #16: 0x000000018eff74e0 UIKitCore`-[UIGestureEnvironment _updateForEvent:window:] + 728
    frame #17: 0x000000018f4bb4cc UIKitCore`-[UIWindow sendEvent:] + 3796
    frame #18: 0x000000018f496b0c UIKitCore`-[UIApplication sendEvent:] + 744
    frame #19: 0x000000018f519078 UIKitCore`__dispatchPreprocessedEventFromEventQueue + 1032
    frame #20: 0x000000018f51d818 UIKitCore`__processEventQueue + 6440
    frame #21: 0x000000018f514afc UIKitCore`__eventFetcherSourceCallback + 156
    frame #22: 0x000000018cb8dbf0 CoreFoundation`__CFRUNLOOP_IS_CALLING_OUT_TO_A_SOURCE0_PERFORM_FUNCTION__ + 24
    frame #23: 0x000000018cb8daf0 CoreFoundation`__CFRunLoopDoSource0 + 204
    frame #24: 0x000000018cb8ce38 CoreFoundation`__CFRunLoopDoSources0 + 256
    frame #25: 0x000000018cb873e0 CoreFoundation`__CFRunLoopRun + 776
    frame #26: 0x000000018cb86ba0 CoreFoundation`CFRunLoopRunSpecific + 572
    frame #27: 0x00000001a38ec598 GraphicsServices`GSEventRunModal + 160
    frame #28: 0x000000018f4782f4 UIKitCore`-[UIApplication _run] + 1052
    frame #29: 0x000000018f47d874 UIKitCore`UIApplicationMain + 164
    frame #30: 0x00000001020b58c0 VLC for iOS`main(argc=1, argv=0x000000016dd977c0) at main.m:20:16
    frame #31: 0x000000018c865568 libdyld.dylib`start + 4
(lldb) 
Les refs à un UIMultiSelectInteraction,_UICollectionViewMultiSelectController, ou encore UIMultiSelectInteraction dans cette stack m’ont donné l’indice.

Du coup j’ai regardé quelle était la stack d’un didSelectItemAt avec des cellulles sans UIScrollView et sans tapGesture :

(lldb) thread backtrace
* thread #1, queue = 'com.apple.main-thread', stop reason = breakpoint 6.1
  * frame #0: 0x000000010028718c VLC for iOS`MediaCategoryViewController.collectionView(collectionView=0x00000001070fc800, indexPath=2 indices, self=0x0000000107975a00) at MediaCategoryViewController.swift:841:9
    frame #1: 0x0000000100287220 VLC for iOS`@objc MediaCategoryViewController.collectionView(_:didSelectItemAt:) at <compiler-generated>:0
    frame #2: 0x000000018ebf2c58 UIKitCore`-[UICollectionView _selectItemAtIndexPath:animated:scrollPosition:notifyDelegate:deselectPrevious:performCustomSelectionAction:] + 892
    frame #3: 0x000000018ec1b9d0 UIKitCore`-[UICollectionView touchesEnded:withEvent:] + 604
    frame #4: 0x000000018f4ab998 UIKitCore`forwardTouchMethod + 316
    frame #5: 0x000000018f4aba84 UIKitCore`-[UIResponder touchesEnded:withEvent:] + 60
    frame #6: 0x000000018f4ab998 UIKitCore`forwardTouchMethod + 316
    frame #7: 0x000000018f4aba84 UIKitCore`-[UIResponder touchesEnded:withEvent:] + 60
    frame #8: 0x000000018eff9350 UIKitCore`_UIGestureEnvironmentUpdate + 6676
    frame #9: 0x000000018eff74e0 UIKitCore`-[UIGestureEnvironment _updateForEvent:window:] + 728
    frame #10: 0x000000018f4bb4cc UIKitCore`-[UIWindow sendEvent:] + 3796
    frame #11: 0x000000018f496b0c UIKitCore`-[UIApplication sendEvent:] + 744
    frame #12: 0x000000018f519078 UIKitCore`__dispatchPreprocessedEventFromEventQueue + 1032
    frame #13: 0x000000018f51d818 UIKitCore`__processEventQueue + 6440
    frame #14: 0x000000018f514afc UIKitCore`__eventFetcherSourceCallback + 156
    frame #15: 0x000000018cb8dbf0 CoreFoundation`__CFRUNLOOP_IS_CALLING_OUT_TO_A_SOURCE0_PERFORM_FUNCTION__ + 24
    frame #16: 0x000000018cb8daf0 CoreFoundation`__CFRunLoopDoSource0 + 204
    frame #17: 0x000000018cb8ce38 CoreFoundation`__CFRunLoopDoSources0 + 256
    frame #18: 0x000000018cb873e0 CoreFoundation`__CFRunLoopRun + 776
    frame #19: 0x000000018cb86ba0 CoreFoundation`CFRunLoopRunSpecific + 572
    frame #20: 0x00000001a38ec598 GraphicsServices`GSEventRunModal + 160
    frame #21: 0x000000018f4782f4 UIKitCore`-[UIApplication _run] + 1052
    frame #22: 0x000000018f47d874 UIKitCore`UIApplicationMain + 164
    frame #23: 0x0000000100165824 VLC for iOS`main(argc=1, argv=0x000000016fce77c0) at main.m:20:16
    frame #24: 0x000000018c865568 libdyld.dylib`start + 4

J’ai ensuite cherché dans la codebase les flags qui pouvaient trigger de la multi selection, je les ai passé à false et le problème avait disparu.
Après j’ai creusé un peu et j’ai constaté qu’une PanGesture privée d’Apple du nom de _UIMultiSelectOneFingerPanGesture avait un touchEvent valide dans le passage de didSelectItemAt après avoir déclenché un edit.
Ce gesture a l’air de prendre la priorité sur toutes les autres, mais n’est pas sensé être actif si le view controller ou si la collection view on leur flag isEditing à false. C’est le cas si tu ne touche pas aux flags d’editing, _UIMultiSelectOneFingerPanGesture est là mais aucun touchEvent n’est créé.
J’en conclus donc un gros bug UIKit qui tâche puisque normalement si allowsMultipleSelectionDuringEditing est à true alors que le isEditing est à false,  le _UIMultiSelectOneFingerPanGesture ne devrait pas remonter de touch event.


Actually having a `collectionView.allowsMultipleSelectionDuringEditing == true` when the `UICollectionViewController.isEditing == false` still triggers a private typed `_UIMultiSelectOneFingerPanGesture` event, discarding cells' `UIScrollView`'s pan gesture events

A quick workaround can be to set `collectionView.allowsMultipleSelectionDuringEditing == isEditing` as soon as `UICollectionViewController.isEditing` is set to `false`

This sounds like a UIKit bug.
