//
//  ViewController.swift
//  scroll-view-in-collection-view-cell-test
//
//  Created by umxprime on 31/01/2022.
//

import UIKit

class ViewController: UIViewController {

    private var collectionViewController: MyCollectionViewController?
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var gotBugButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        guard let collectionViewController = destination as? MyCollectionViewController else {
            return
        }
        self.collectionViewController = collectionViewController
    }

    @IBAction func didTapEditButton(_ sender: Any) {
        guard let collectionViewController = self.collectionViewController else {
            return
        }
        var buttonTitle = "Edit"
        if collectionViewController.switchEdition() {
            buttonTitle = "Done"
        }
        editButton.setTitle(buttonTitle, for: .normal)
        editButton.setTitle(buttonTitle, for: .selected)
    }
    @IBAction func didTapGotBugButton(_ sender: Any) {
        guard let collectionViewController = self.collectionViewController else {
            return
        }
        var buttonTitle = "Nah Bug"
        if collectionViewController.switchWannaBug() {
            buttonTitle = "Got Bug"
        }
        gotBugButton.setTitle(buttonTitle, for: .normal)
        gotBugButton.setTitle(buttonTitle, for: .selected)
    }
}

