//
//  MyCollectionViewController.swift
//  scroll-view-in-collection-view-cell-test
//
//  Created by umxprime on 31/01/2022.
//

import UIKit

private let reuseIdentifier = "MyCollectionViewCell"

class MyCollectionViewController: UICollectionViewController {
    private var wannaBug: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        let nib = UINib.init(nibName: "MyCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.allowsSelection = true
        self.collectionView.allowsMultipleSelection = true

        // Do any additional setup after loading the view.
    }

    private func updateCollectionViewSelectionBehaviour(_ editing: Bool) {
        if #available(iOS 14.0, *) {
            self.collectionView.allowsMultipleSelectionDuringEditing = wannaBug ? true : isEditing
        } else {
            // Fallback on earlier versions
        }
    }

    public func switchEdition() -> Bool {
        let editing = !isEditing
        self.setEditing(editing, animated: true)
        self.updateCollectionViewSelectionBehaviour(editing)
        return editing
    }

    public func switchWannaBug() -> Bool {
        self.wannaBug = !self.wannaBug
        self.updateCollectionViewSelectionBehaviour(isEditing)
        return self.wannaBug
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 100
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        guard let cell = cell as? MyCollectionViewCell else {
            return cell
        }

        cell.label.text = "Cell \(indexPath.row)"
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("isEditing \( isEditing ? "true" : "false" )")
        if #available(iOS 14.0, *) {
            print("collectionView.allowsMultipleSelectionDuringEditing \( collectionView.allowsMultipleSelectionDuringEditing ? "true" : "false" )")
        }
        if let objType = NSClassFromString("_UIMultiSelectOneFingerPanGesture") {
            if let gestureRecognizers = collectionView.gestureRecognizers {
                for gesture in gestureRecognizers {
                    if gesture.isKind(of: objType) {
                        let numberOfTouches = gesture.numberOfTouches
                        if numberOfTouches > 0 {
                            // Given isEditing == false
                            // This shouldn't happen when
                            // collectionView.allowsMultipleSelectionDuringEditing == true
                            print("_UIMultiSelectOneFingerPanGesture detected with \(numberOfTouches) touches")
                        }
                    }
                }
            }
        }
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
