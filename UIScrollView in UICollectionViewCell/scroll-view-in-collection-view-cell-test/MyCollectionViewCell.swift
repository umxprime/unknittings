//
//  MyCollectionViewCell.swift
//  scroll-view-in-collection-view-cell-test
//
//  Created by umxprime on 31/01/2022.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
